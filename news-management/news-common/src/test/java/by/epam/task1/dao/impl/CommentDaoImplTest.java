package by.epam.task1.dao.impl;

import by.epam.task1.dao.CommentDao;
import by.epam.task1.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Darya_Yeuzhyk on 17/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = {"/context-test.xml"})
@DatabaseSetup(value = "CommentDaoImplTest.xml")
@DatabaseTearDown(value = "CommentDaoImplTest.xml",
        type = DatabaseOperation.DELETE_ALL)
public class CommentDaoImplTest {
    @Inject
    private CommentDao commentDao;

    @Test
    public void testCreate() throws Exception {
        Comment expectedComment = new Comment();
        expectedComment.setNewsId(1L);
        expectedComment.setCommentText("text");
        expectedComment.setCreationDate(new Date());
        Long commentId = commentDao.create(expectedComment);
        expectedComment.setCommentId(commentId);
        Comment actualComment = commentDao.read(commentId);

        assertEquals(expectedComment, actualComment);
    }

    @Test
    public void testRead() throws Exception {
        Long commentId = 1L;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date creationDate = format.parse("2015-05-13 14:35:10");
        Comment expectedComment = new Comment();
        expectedComment.setCommentId(commentId);
        expectedComment.setNewsId(2L);
        expectedComment.setCommentText("comment1");
        expectedComment.setCreationDate(creationDate);
        Comment actualComment = commentDao.read(commentId);

        assertEquals(expectedComment, actualComment);
    }

    @Test
    public void testUpdate() throws Exception {
        Long commentId = 1L;
        Comment expectedComment = new Comment();
        expectedComment.setCommentId(commentId);
        expectedComment.setCommentText("comment123");
        commentDao.update(expectedComment);
        Comment actualComment = commentDao.read(commentId);

        assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
    }

    @Test
    public void testDelete() throws Exception {
        Long commentId = 1L;
        commentDao.delete(commentId);
        Comment actualComment = commentDao.read(commentId);

        assertNull(actualComment);
    }

    @Test
    public void testReadListByNews() throws Exception {
        List<Comment> comments = commentDao.readListByNews(2L);
        int expectedSize = 2;
        Long commentId1 = 2L;
        Long commentId2 = 1L;

        assertEquals(expectedSize, comments.size());
        assertEquals(commentId1, comments.get(0).getCommentId());
        assertEquals(commentId2, comments.get(1).getCommentId());
    }

    @Test
    public void testDeleteByNews() throws Exception {
        Long newsId = 2L;
        commentDao.deleteByNews(newsId);
        List<Comment> comments = commentDao.readListByNews(newsId);

        assertTrue(comments.isEmpty());
    }
}
