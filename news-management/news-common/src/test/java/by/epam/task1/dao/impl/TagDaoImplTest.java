package by.epam.task1.dao.impl;

import by.epam.task1.dao.TagDao;
import by.epam.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Darya_Yeuzhyk on 17/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = {"/context-test.xml"})
@DatabaseSetup(value = "TagDaoImplTest.xml")
@DatabaseTearDown(value = "TagDaoImplTest.xml",
        type = DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {
    @Inject
    private TagDao tagDao;

    @Test
    public void testCreate() throws Exception {
        Tag expectedTag = new Tag();
        expectedTag.setTagName("tag_name");
        Long tagId = tagDao.create(expectedTag);
        expectedTag.setTagId(tagId);
        Tag actualTag = tagDao.read(tagId);

        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testRead() throws Exception {
        Long tagId = 1L;
        Tag expectedTag = new Tag();
        expectedTag.setTagId(tagId);
        expectedTag.setTagName("tag_name1");
        Tag actualTag = tagDao.read(tagId);

        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testUpdate() throws Exception {
        Long tagId = 1L;
        Tag expectedTag = new Tag();
        expectedTag.setTagId(tagId);
        expectedTag.setTagName("tag_name123");
        tagDao.update(expectedTag);
        Tag actualTag = tagDao.read(tagId);

        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testDelete() throws Exception {
        Long tagId = 2L;
        tagDao.delete(tagId);
        Tag actualTag = tagDao.read(tagId);

        assertNull(actualTag);
    }

    @Test
    public void testReadListAll() throws Exception {
        List<Tag> tags = tagDao.readListAll();
        int expectedSize = 4;

        assertEquals(expectedSize, tags.size());
    }

    @Test
    public void testReadList() throws Exception {
        int from = 3;
        int to = 5;
        List<Tag> tags = tagDao.readList(from, to);
        int expectedSize = 2;
        Long tagId1 = 3L;
        Long tagId2 = 4L;

        assertEquals(expectedSize, tags.size());
        assertEquals(tagId1, tags.get(0).getTagId());
        assertEquals(tagId2, tags.get(1).getTagId());
    }

    @Test
    public void testReadListByNews() throws Exception {
        List<Tag> tags = tagDao.readListByNews(1L);
        int expectedSize = 2;
        Long tagId1 = 3L;
        Long tagId2 = 4L;

        assertEquals(expectedSize, tags.size());
        assertEquals(tagId1, tags.get(0).getTagId());
        assertEquals(tagId2, tags.get(1).getTagId());
    }


    @Test
    public void testRemoveNewsBinding() throws Exception {
        Long tagId = 4L;
        Long newsId = 1L;
        tagDao.removeNewsBinding(tagId);
        List<Tag> tags = tagDao.readListByNews(newsId);
        int expectedSize = 1;
        Long expectedTagId = 3L;

        assertEquals(expectedSize, tags.size());
        assertEquals(expectedTagId, tags.get(0).getTagId());
    }
}
