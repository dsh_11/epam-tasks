package by.epam.task1;

import by.epam.task1.service.AuthorService;
import by.epam.task1.service.NewsTOService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Darya_Yeuzhyk on 18/03/2016.
 */
public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        NewsTOService newsTOService = (NewsTOService)context.getBean("newsTOServiceImpl");
        System.out.println(newsTOService.getClass());
    }
}
