package by.epam.task1.dao;

/**
 * Created by Darya_Yeuzhyk on 10/02/2016.
 */

import by.epam.task1.exception.DaoException;

/**
 * Generic interface which provides CRUD methods for
 * working with database for instance of type <code>T</code>.
 *
 * @param <T> type of instance
 * @author Darya Yeuzhyk
 */
public interface GenericCRUDDao<T> {
    /**
     * Creates an instance of type <code>T</code>.
     * Returns id of created instance.
     *
     * @param obj instance which is to be created
     * @return id of created instance
     * @throws DaoException if SQLException is thrown
     */
    Long create(T obj) throws DaoException;

    /**
     * Returns instance of type <code>T</code> by id.
     *
     * @param id id of instance which is to be returned
     * @return instance of type <code>T</code> by id or
     *         <code>null</code> if there is no instance found
     * @throws DaoException if SQLException is thrown
     */
    T read(Long id) throws DaoException;

    /**
     * Updates instance <code>obj</code>.
     *
     * @param obj instance which is to be updated
     * @throws DaoException if SQLException is thrown
     */
    void update(T obj) throws DaoException;

    /**
     * Deletes instance by id <code>id</code>.
     *
     * @param id id of instance to be deleted.
     * @throws DaoException if SQLException is thrown
     */
    void delete(Long id) throws DaoException;
}
