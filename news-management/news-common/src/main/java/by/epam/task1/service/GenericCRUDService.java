package by.epam.task1.service;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */

import by.epam.task1.exception.ServiceException;

/**
 * Generic interface which provides CRUD methods for
 * working with DAO layer for instance of type <code>T</code>.
 *
 * @param <T> type of instance
 * @author Darya Yeuzhyk
 */
public interface GenericCRUDService<T> {
    /**
     * Creates an instance of type <code>T</code>.
     * Returns id of created instance.
     *
     * @param obj instance which is to be created
     * @return id of created instance
     * @throws ServiceException if DaoException is thrown
     */
    Long create(T obj) throws ServiceException;

    /**
     * Returns instance of type <code>T</code> by id.
     *
     * @param id id of instance which is to be returned
     * @return instance of type <code>T</code> by id or
     *         <code>null</code> if there is no instance found
     * @throws ServiceException if DaoException is thrown
     */
    T read(Long id) throws ServiceException;

    /**
     * Updates instance <code>obj</code>.
     *
     * @param obj instance which is to be updated
     * @throws ServiceException if DaoException is thrown
     */
    void update(T obj) throws ServiceException;

    /**
     * Deletes instance by id <code>id</code>.
     *
     * @param id id of instance to be deleted.
     * @throws ServiceException if DaoException is thrown
     */
    void delete(Long id) throws ServiceException;
}
