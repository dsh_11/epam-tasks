package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */
@Service
public class NewsTOServiceImpl implements NewsTOService {
    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Transactional(rollbackFor = { Exception.class })
    public Long create(NewsTO newsTO) throws ServiceException {
        News news = newsTO.getNews();
        Author author = newsTO.getAuthor();
        List<Tag> tags = newsTO.getTags();
        Long newsId = newsService.create(news);
        newsService.addAuthorBinding(newsId, author.getAuthorId());
        List<Long> tagIds = new ArrayList<>();
        for (Tag tag : tags) {
            tagIds.add(tag.getTagId());
        }
        newsService.addTagsBinding(newsId, tagIds);
        return newsId;
    }

    public NewsTO read(Long newsId) throws ServiceException {
        NewsTO newsTO = new NewsTO();
        News news = newsService.read(newsId);
        Author author = authorService.readByNews(newsId);
        List<Tag> tags = tagService.readListByNews(newsId);
        List<Comment> comments = commentService.readListByNews(newsId);
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);
        newsTO.setComments(comments);
        return newsTO;
    }

    @Transactional(rollbackFor = { Exception.class })
    public void update(NewsTO newsTO) throws ServiceException {
        News news = newsTO.getNews();
        long newsId = news.getNewsId();
        Author author = newsTO.getAuthor();
        List<Tag> tags = newsTO.getTags();
        try {
            List<Long> tagsIds = new ArrayList<>();
            for (Tag tag : tags) {
                tagsIds.add(tag.getTagId());
            }
            newsService.removeTagBinding(newsId);
            newsService.addTagsBinding(newsId, tagsIds);
            Author oldAuthor = authorService.readByNews(newsId);
            if (!author.getAuthorId().equals(oldAuthor.getAuthorId())) {
                newsService.removeAuthorBinding(newsId);
                newsService.addAuthorBinding(newsId, author.getAuthorId());
            }
            newsService.update(news);
        } catch (ServiceException e) {
            throw new ServiceException("", e);
        }
    }

    @Transactional(rollbackFor = { Exception.class })
    public void delete(Long newsId) throws ServiceException {
        newsService.removeAuthorBinding(newsId);
        newsService.removeTagBinding(newsId);
        commentService.deleteByNews(newsId);
        newsService.delete(newsId);
    }

    public List<NewsTO> readList(int page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.readList(page);
        for (News news : newsList) {
            NewsTO newsTO = new NewsTO();
            Long newsId = news.getNewsId();
            List<Tag> tags = tagService.readListByNews(newsId);
            Author author = authorService.readByNews(newsId);
            List<Comment> comments = commentService.readListByNews(newsId);
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTags(tags);
            newsTO.setComments(comments);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    public List<NewsTO> search(SearchCriteriaTO searchCriteria, int page) throws ServiceException {
        List<NewsTO> newsTOList = new ArrayList<>();
        List<News> newsList = newsService.search(searchCriteria, page);
        for (News news : newsList) {
            NewsTO newsTO = new NewsTO();
            List<Tag> tags = tagService.readListByNews(news.getNewsId());
            Author author = authorService.readByNews(news.getNewsId());
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTags(tags);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }

    public int getPagesNumber() throws ServiceException {
        long newsNumber = newsService.getNumber();
        int newsNumberPerPage = NewsServiceImpl.NEWS_NUMBER_PER_PAGE;
        return (int) Math.ceil((double) newsNumber / newsNumberPerPage);
    }
}
