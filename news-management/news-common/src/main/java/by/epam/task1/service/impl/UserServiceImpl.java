package by.epam.task1.service.impl;

import by.epam.task1.dao.UserDao;
import by.epam.task1.exception.DaoException;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Darya_Yeuzhyk on 29/03/2016.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    public boolean userValid(String login, String password) throws ServiceException {
        try {
            return userDao.userValid(login, password);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }
}
