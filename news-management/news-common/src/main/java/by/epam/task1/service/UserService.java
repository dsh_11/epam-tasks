package by.epam.task1.service;

import by.epam.task1.exception.ServiceException;

/**
 * Created by Darya_Yeuzhyk on 29/03/2016.
 */
public interface UserService {
    boolean userValid(String login, String password) throws ServiceException;
}
