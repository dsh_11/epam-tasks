package by.epam.task1.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by Daria on 24.04.2016.
 */
public class ServiceLogger {
    private static final Logger logger = LogManager.getRootLogger();

    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        StringBuilder message = new StringBuilder();

        message.append(joinPoint.toLongString());
        message.append("\n");

        Object[] parameters = joinPoint.getArgs();
        message.append("Parameters:\n");
        for (Object parameter : parameters) {
            message.append("\t");
            message.append(parameter.toString());
            message.append("\n");
        }

        try {
            Object returnedObject = joinPoint.proceed();
            message.append("Returned object:\n\t");
            message.append(returnedObject);
            message.append("\n\n");

            return returnedObject;
        } catch (Throwable e) {
            message.append("Exception:\n\t");
            message.append(e);
            message.append("\n\n");

            throw e;
        } finally {
            logger.debug(message.toString());
        }
    }
}
