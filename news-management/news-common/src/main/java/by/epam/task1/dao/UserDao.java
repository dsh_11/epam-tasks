package by.epam.task1.dao;

import by.epam.task1.domain.User;
import by.epam.task1.exception.DaoException;

/**
 * Created by Darya_Yeuzhyk on 25/03/2016.
 */
public interface UserDao extends GenericCRUDDao<User> {
    boolean userValid(String login, String password) throws DaoException;
}
