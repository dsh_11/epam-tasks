package by.epam.task1.service;

import by.epam.task1.domain.NewsTO;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */

/**
 * This interface provides methods for working with newsTO.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.News
 */
public interface NewsTOService {
    /**
     * Creates an instance of type NewsTO.
     *
     * @param newsTO instance which is bo created
     * @return id of created news
     * @throws ServiceException if DaoException is thrown
     */
    Long create(NewsTO newsTO) throws ServiceException;

    /**
     * Returns newsTO by id of news.
     *
     * @param newsId id of news
     * @return newsTO by id of news
     * @throws ServiceException if DaoException is thrown
     */
    NewsTO read(Long newsId) throws ServiceException;

    /**
     * Updates news.
     *
     * @param newsTO news to be updated
     * @throws ServiceException if DaoException is thrown
     */
    void update(NewsTO newsTO) throws ServiceException;

    /**
     * Removes newsTO by id of news <code>newsId</code>,
     *
     * @param newsId id of news
     * @throws ServiceException if DaoException is thrown
     */
    void delete(Long newsId) throws ServiceException;

    /**
     * Returns a list of newsTO for page <code>page</code>.
     *
     * @param page number of page for which newsTO are to be returned
     * @return a list of news
     * @throws ServiceException if DaoException is thrown
     */
    List<NewsTO> readList(int page) throws ServiceException;

    /**
     * Returns a list of newsTO satisfying <code>searchCriteria</code> for
     * page <code>page</code>.
     *
     * @param searchCriteria search criteria by which news are to be searched
     * @param page number of page for which newsTO are to be returned
     * @return a list of newsTO
     * @throws ServiceException if DaoException is thrown
     */
    List<NewsTO> search(SearchCriteriaTO searchCriteria, int page) throws ServiceException;

    /**
     * Returns number of pages.
     *
     * @return number of pages
     * @throws ServiceException if DaoException is thrown
     */
    int getPagesNumber() throws ServiceException;
}
