package by.epam.task1.exception;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class DaoException extends Exception {
    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
