package by.epam.task1.service;

import by.epam.task1.domain.Comment;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 12/02/2016.
 */

/**
 * This interface provides methods for working with DAO layer
 * for instance <code>Comment</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Comment
 * @see by.epam.task1.service.GenericCRUDService
 */
public interface CommentService extends GenericCRUDService<Comment> {
    /**
     * Returns list of comments by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which comments are to be returned
     * @return list of comments by news id <code>newsId</code>
     * @throws ServiceException if DaoException is thrown
     */
    List<Comment> readListByNews(Long newsId) throws ServiceException;

    /**
     * Removes all comments by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which comments are to be deleted
     * @throws ServiceException if DaoException is thrown
     */
    void deleteByNews(Long newsId) throws ServiceException;
}
