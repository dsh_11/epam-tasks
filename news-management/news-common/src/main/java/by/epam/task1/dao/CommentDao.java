package by.epam.task1.dao;

import by.epam.task1.domain.Comment;
import by.epam.task1.exception.DaoException;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */

/**
 * This interface provides methods for working with database
 * for instance <code>Comment</code>.
 *
 * @author Darya Yeuzhyk
 * @see by.epam.task1.domain.Comment
 * @see by.epam.task1.dao.GenericCRUDDao
 */
public interface CommentDao extends GenericCRUDDao<Comment> {
    /**
     * Returns list of comments by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which comments are to be returned
     * @return list of comments by news id <code>newsId</code>
     * @throws DaoException if SQLException is thrown
     */
    List<Comment> readListByNews(Long newsId) throws DaoException;

    /**
     * Removes all comments by news with id <code>newsId</code>.
     *
     * @param newsId id of news for which comments are to be deleted
     * @throws DaoException if SQLException is thrown
     */
    void deleteByNews(Long newsId) throws DaoException;
}
