package by.epam.task1.dao.impl;

import by.epam.task1.dao.UserDao;
import by.epam.task1.domain.User;
import by.epam.task1.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Darya_Yeuzhyk on 28/03/2016.
 */
@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    private DataSource dataSource;

    private static final String SQL_USER_CREATE = "INSERT INTO users(user_name, " +
            "login, password) VALUES(?, ?, ?)";
    private static final String SQL_USER_READ = "SELECT user_name, login, password " +
            "FROM users WHERE user_id = ?";
    private static final String SQL_USER_UPDATE = "UPDATE users SET user_name = ?, " +
            "login = ?, password = ? WHERE user_id = ?";
    private static final String SQL_USER_DELETE = "DELETE FROM users WHERE user_id = ?";
    private static final String SQL_USER_USER_VALID = "SELECT user_id FROM users " +
            "WHERE login = ? AND password = ?";

    private static final String USER_ID_FIELD_NAME = "user_id";

    public Long create(User user) throws DaoException {
        String[] generatedKeys = { USER_ID_FIELD_NAME };
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(
                SQL_USER_CREATE, generatedKeys)) {
            statement.setNString(1, user.getUserName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                Long userId = null;
                if (resultSet.next()) {
                    userId = resultSet.getLong(1);
                }
                return userId;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public User read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_USER_READ)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                User user = null;
                if (resultSet.next()) {
                    user = new User();
                    user.setUserId(id);
                    user.setUserName(resultSet.getNString(1));
                    user.setLogin(resultSet.getString(2));
                    user.setPassword(resultSet.getString(3));
                }
                return user;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void update(User user) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_USER_UPDATE)) {
            statement.setNString(1, user.getUserName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_USER_DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public boolean userValid(String login, String password) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_USER_USER_VALID)) {
            statement.setString(1, login);
            statement.setString(2, password);
            try (ResultSet resultSet = statement.executeQuery()) {
                boolean result = false;
                if (resultSet.next()) {
                    result = true;
                }
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
