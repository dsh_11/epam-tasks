package by.epam.task1.domain;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class Tag implements Serializable {
    private Long tagId;

    @Size(min = 3, max = 15)
    private String tagName;

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (getClass() != o.getClass()) {
            return false;
        }

        Tag obj = (Tag) o;

        return ((tagId == obj.getTagId()) ||
                        (tagId != null && tagId.equals(obj.getTagId()))) &&
                ((tagName == obj.getTagName()) ||
                        (tagName != null && tagName.equals(obj.getTagName())));
    }

    public int hashCode() {
        int result = tagId != null ? tagId.hashCode() : 0;
        result = 7 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }

    public String toString() {
        StringBuilder result = new StringBuilder(getClass().toString());
        result.append(" { tagId: ");
        result.append(tagId);
        result.append(", tagName: ");
        result.append(tagName);
        result.append(" }");

        return result.toString();
    }
}
