package by.epam.task1.domain;

import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 09/02/2016.
 */
public class SearchCriteriaTO {
    private Long authorId;
    private List<Long> tagIds;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }
}
