<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <title>News list</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/multiple-select.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pagination.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/news-list.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css"/>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/multiple-select.js"></script>
    </head>
    <body>
        <div class="header">
            <jsp:include page="header.jsp"/>
        </div>

        <div class="content">
            <div class="news-list-search">
                <form action="${pageContext.request.contextPath}/news-management/search" method="POST">
                    <select name="authorId">
                        <option value="">Select author</option>
                        <c:forEach items="${authorsList}" var="author">
                            <c:if test="${author.authorId eq criteria.authorId}">
                                <option value="${author.authorId}" selected>${author.authorName}</option>
                            </c:if>
                            <c:if test="${author.authorId ne criteria.authorId}">
                                <option value="${author.authorId}">${author.authorName}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <select name="tagsIds" style="width: 150px" id="tags" multiple>
                        <c:forEach items="${tagsList}" var="tag">
                            <c:if test="${fn:contains(criteria.tagIds, tag.tagId)}">
                                <option value="${tag.tagId}" selected>${tag.tagName}</option>
                            </c:if>
                            <c:if test="${not fn:contains(criteria.tagIds, tag.tagId)}">
                                <option value="${tag.tagId}">${tag.tagName}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Search"/>
                </form>
                <form action="${pageContext.request.contextPath}/news-management/reset" method="POST">
                    <input type="submit" value="Reset"/>
                </form>
            </div>

            <ul style="list-style-type: none;">
                <c:forEach items="${newsList}" var="news">
                    <li class="news-item">
                        <div>
                            <div>
                                <ul style="padding-left: 0;">
                                    <li class="news-title">
                                        <a class="news-title"
                                           href="${pageContext.request.contextPath}/news-management/news/${news.news.newsId}">
                                                ${news.news.title}
                                        </a>
                                    </li>
                                    <li class="author-name">(by ${news.author.authorName})</li>
                                    <li class="date">
                                        <fmt:formatDate type="date" value="${news.news.creationDate}" />
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <span>${news.news.shortText}</span>
                            </div>

                            <div align="right" style="margin-right: 30px;">
                                <ul class="news-bottom">
                                    <c:if test="${not empty news.tags}">
                                        <li>
                                            <ul>
                                                <c:forEach items="${news.tags}" var="tag">
                                                    <li>${tag.tagName}</li>
                                                </c:forEach>
                                            </ul>
                                        </li>
                                    </c:if>
                                    <li style="margin: 0 5px 0 10px;">
                                        Comments(${fn:length(news.comments)})
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </c:forEach>
            </ul>

            <div style="clear: both" class="pages">
                <ul class="pagination">
                    <c:forEach begin="1" end="${pagesNumber}" var="i">
                        <c:choose>
                            <c:when test="${currentPage == i}">
                                <li>
                                    <span id="page-active">${i}</span>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a id="page-inactive" href="${pageContext.request.contextPath}/news-management/news-list/${i}">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </div>
        </div>

        <div class="footer">
            <jsp:include page="footer.jsp"/>
        </div>
    </body>
</html>

<script>
    $('#tags').multipleSelect();
</script>
