<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Page not found.</title>
    </head>
    <body>
        <h2>Requested page was not found.</h2>
        <h3>Go to <a href="${pageContext.request.contextPath}/">main</a> page.</h3>
    </body>
</html>
