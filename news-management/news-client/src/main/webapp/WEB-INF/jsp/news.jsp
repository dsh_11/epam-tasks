<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
    <head>
        <title>${news.news.title}</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/news.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css"/>
    </head>
    <body>
        <div class="header">
            <jsp:include page="header.jsp"/>
        </div>

        <div class="content">
            <div class="news">
                <h4>${news.news.title}</h4>
                <div id="author-name">(by ${news.author.authorName})</div>
                <div id="creation-date"><fmt:formatDate type="date" value="${news.news.creationDate}" /></div>
                <div>
                    <p>
                        ${news.news.fullText}
                    </p>
                </div>
                <div>
                    <form action="${pageContext.request.contextPath}/news-management/add-comment" method="POST">
                        <div>
                            <textarea name="commentText" id="comment-area" rows="10" cols="40"></textarea>
                        </div>
                        <div>
                            <input name="submit" type="submit" value="Post comment"/>
                        </div>
                        <input type="hidden" name="newsId" value="${news.news.newsId}"/>
                    </form>
                </div>
                <c:if test="${not empty news.comments}">
                    <div>
                        <ul class="comments">
                            <c:forEach items="${news.comments}" var="comment">
                                <li class="comment-item">
                                    <div>
                                        <fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
                                    </div>
                                    <div class="comment">
                                            ${comment.commentText}
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
            </div>
        </div>

        <div class="footer">
            <jsp:include page="footer.jsp"/>
        </div>
    </body>
</html>
