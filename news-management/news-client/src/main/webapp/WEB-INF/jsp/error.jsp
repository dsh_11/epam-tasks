<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Error</title>
    </head>
    <body>
        <h2>Some error has occurred.</h2>
        <h3>Go to <a href="${pageContext.request.contextPath}/">main</a> page.</h3>
    </body>
</html>
