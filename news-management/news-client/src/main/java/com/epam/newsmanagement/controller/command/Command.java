package com.epam.newsmanagement.controller.command;

import by.epam.task1.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Daria on 25.04.2016.
 */
public interface Command {
    String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException;
}
