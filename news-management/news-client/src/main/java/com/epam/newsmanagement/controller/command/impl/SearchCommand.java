package com.epam.newsmanagement.controller.command.impl;

import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.NewsTOService;
import com.epam.newsmanagement.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daria on 25.04.2016.
 */
@Component
public class SearchCommand implements Command {

    private static final String AUTHOR_ID_PARAM = "authorId";
    private static final String TAGS_IDS_PARAM = "tagsIds";
    private static final String SEARCH_CRITERIA_PARAM = "criteria";

    private static final String VIEW_NAME = "redirect:/";

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String authorIdParam = request.getParameter(AUTHOR_ID_PARAM);
        Long authorId = null;
        if ((authorIdParam != null) && !authorIdParam.isEmpty()) {
            authorId = Long.valueOf(authorIdParam);
        }

        String[] tagsIdsParam = request.getParameterValues(TAGS_IDS_PARAM);
        List<Long> tagsIds = null;
        if (tagsIdsParam != null) {
            tagsIds = new ArrayList<>();
            for (String tagIdParam : tagsIdsParam) {
                Long tagId = Long.valueOf(tagIdParam);
                tagsIds.add(tagId);
            }
        }

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIds(tagsIds);

        request.getSession().setAttribute(SEARCH_CRITERIA_PARAM, searchCriteria);

        return VIEW_NAME;
    }
}
