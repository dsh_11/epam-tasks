package com.epam.newsmanagement.controller.command.impl;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.NewsTO;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import by.epam.task1.service.NewsTOService;
import by.epam.task1.service.TagService;
import com.epam.newsmanagement.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Daria on 25.04.2016.
 */
@Component
public class NewsListCommand implements Command {

    private static final String PAGES_NUMBER_PARAM = "pagesNumber";
    private static final String CURRENT_PAGE_PARAM = "currentPage";
    private static final String NEWS_LIST_PARAM = "newsList";
    private static final String AUTHORS_LIST_PARAM = "authorsList";
    private static final String TAGS_LIST_PARAM = "tagsList";
    private static final String SEARCH_CRITERIA_PARAM = "criteria";

    private static final String VIEW_NAME = "/WEB-INF/jsp/news_list.jsp";

    @Autowired
    private NewsTOService newsTOService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String pathInfo = request.getPathInfo();
        int page = 1;
        if (pathInfo != null) {
            String[] paths = pathInfo.split("/");
            if (paths.length > 2) {
                String pageParam = paths[2];
                page = Integer.valueOf(pageParam);
            }
        }

        SearchCriteriaTO searchCriteria =
                (SearchCriteriaTO) request.getSession().getAttribute(SEARCH_CRITERIA_PARAM);
        List<NewsTO> newsList;
        if (searchCriteria == null) {
            newsList = newsTOService.readList(page);
        } else {
            newsList = newsTOService.search(searchCriteria, page);
        }
        List<Author> authorsList = authorService.readListAll();
        List<Tag> tagsList = tagService.readListAll();

        int pagesNumber = newsTOService.getPagesNumber();

        request.setAttribute(PAGES_NUMBER_PARAM, pagesNumber);
        request.setAttribute(CURRENT_PAGE_PARAM, page);
        request.setAttribute(NEWS_LIST_PARAM, newsList);
        request.setAttribute(AUTHORS_LIST_PARAM, authorsList);
        request.setAttribute(TAGS_LIST_PARAM, tagsList);

        return VIEW_NAME;
    }
}
