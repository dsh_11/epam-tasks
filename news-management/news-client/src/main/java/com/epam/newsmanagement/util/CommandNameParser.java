package com.epam.newsmanagement.util;

/**
 * Created by Daria on 25.04.2016.
 */
public class CommandNameParser {

    public static String getBeanName(String commandName) {
        StringBuilder result = new StringBuilder();
        boolean toUpper = false;

        for (int i = 0; i < commandName.length(); i++) {
            char c = commandName.charAt(i);
            if (c == '-') {
                toUpper = true;
            } else {
                result.append(toUpper ? Character.toUpperCase(c) : c);
                toUpper = false;
            }
        }
        result.append("Command");

        return result.toString();
    }
}
