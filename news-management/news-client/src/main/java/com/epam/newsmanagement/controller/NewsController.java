package com.epam.newsmanagement.controller;

import by.epam.task1.exception.ServiceException;
import com.epam.newsmanagement.controller.command.Command;
import com.epam.newsmanagement.util.CommandNameParser;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Daria on 25.04.2016.
 */
public class NewsController extends HttpServlet {

    private static final String DEFAULT_COMMAND_NAME = "news-list";

    private static final String ERROR_VIEW_NAME = "/WEB-INF/jsp/error.jsp";

    private WebApplicationContext context;

    /*public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext servletContext = getServletContext();
        context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }*/

    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        String pathInfo = request.getPathInfo();
        String commandName = DEFAULT_COMMAND_NAME;
        if (pathInfo != null) {
            commandName = pathInfo.split("/")[1];
        }

        String beanName = CommandNameParser.getBeanName(commandName);
        try {
            Command command = (Command) context.getBean(beanName);
            String viewName = command.execute(request, response);
            if (viewName.contains("redirect:")) {
                viewName = viewName.replace("redirect:", "");
                response.sendRedirect(viewName);
            } else {
                request.getRequestDispatcher(viewName).forward(request, response);
            }
        } catch (Exception e) {
            request.getRequestDispatcher(ERROR_VIEW_NAME).forward(request, response);
        }
    }
}
