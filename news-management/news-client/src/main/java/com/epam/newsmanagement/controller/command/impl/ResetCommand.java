package com.epam.newsmanagement.controller.command.impl;

import by.epam.task1.exception.ServiceException;
import com.epam.newsmanagement.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Daria on 26.04.2016.
 */
@Component
public class ResetCommand implements Command {

    private static final String SEARCH_CRITERIA_PARAM = "criteria";

    private static final String VIEW_NAME = "redirect:/";

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        request.getSession().removeAttribute(SEARCH_CRITERIA_PARAM);

        return VIEW_NAME;
    }
}
