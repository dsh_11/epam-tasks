package com.epam.newsmanagement.controller.command.impl;

import by.epam.task1.domain.Comment;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.CommentService;
import com.epam.newsmanagement.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created by Daria on 25.04.2016.
 */
@Component
public class AddCommentCommand implements Command {

    private static final String COMMENT_TEXT_PARAM = "commentText";
    private static final String NEWS_ID_PARAM = "newsId";

    private static final String VIEW_NAME = "redirect:/news-management/news/";

    @Autowired
    private CommentService commentService;

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String commentText = request.getParameter(COMMENT_TEXT_PARAM);
        String newsIdParam = request.getParameter(NEWS_ID_PARAM);
        Long newsId = Long.valueOf(newsIdParam);

        Comment comment = new Comment();
        comment.setCommentText(commentText);
        comment.setNewsId(newsId);
        comment.setCreationDate(new Date());

        commentService.create(comment);
        return VIEW_NAME + newsId;
    }
}
