package com.epam.newsmanagement.controller.command.impl;

import by.epam.task1.domain.NewsTO;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.NewsTOService;
import com.epam.newsmanagement.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Daria on 25.04.2016.
 */
@Component
public class NewsCommand implements Command {

    private static final String NEWS_PARAM = "news";

    private static final String VIEW_NAME = "/WEB-INF/jsp/news.jsp";

    @Autowired
    private NewsTOService newsTOService;

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String pathInfo = request.getPathInfo();
        Long id = null;

        String[] paths = pathInfo.split("/");
        if (paths.length > 2) {
            String idParam = paths[2];
            id = Long.valueOf(idParam);
        }

        NewsTO news = newsTOService.read(id);
        request.setAttribute(NEWS_PARAM, news);
        return VIEW_NAME;
    }
}
