package com.epam.newsmanagement.controller;

import by.epam.task1.domain.Author;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import com.sun.org.apache.regexp.internal.RE;
import oracle.net.aso.a;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 13/04/2016.
 */
@Controller
public class AuthorController {

    private static final String AUTHORS_LIST_PARAM = "authorsList";
    private static final String AUTHOR_PARAM = "author";
    private static final String AUTHOR_FOR_CREATING_PARAM = "authorForCreating";
    private static final String AUTHOR_ID_PARAM = "authorId";

    private static final String AUTHORS_LIST_VIEW_NAME = "authors_list";
    private static final String REDIRECT_AUTHORS_LIST_PARAM = "redirect:/authors-list";

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/authors-list", method = RequestMethod.GET)
    public ModelAndView authorsList(Model model) throws ServiceException {
        List<Author> authorsList = authorService.readListAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(AUTHORS_LIST_PARAM, authorsList);
        if (!model.containsAttribute(AUTHOR_PARAM)) {
            modelAndView.addObject(AUTHOR_PARAM, new Author());
        }
        if (!model.containsAttribute(AUTHOR_FOR_CREATING_PARAM)) {
            modelAndView.addObject(AUTHOR_FOR_CREATING_PARAM, new Author());
        }
        modelAndView.setViewName(AUTHORS_LIST_VIEW_NAME);
        return modelAndView;
    }


    @RequestMapping(value = "/update-author", method = RequestMethod.POST)
    public String updateAuthor(@Valid @ModelAttribute Author author, BindingResult bindingResult,
                               RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.author", bindingResult);
            redirectAttributes.addFlashAttribute(AUTHOR_PARAM, author);
            redirectAttributes.addFlashAttribute(AUTHOR_ID_PARAM, author.getAuthorId());
            return REDIRECT_AUTHORS_LIST_PARAM;
        }

        authorService.update(author);

        return REDIRECT_AUTHORS_LIST_PARAM;
    }

    @RequestMapping(value = "/expire-author", method = RequestMethod.POST)
    public String expireAuthor(@RequestParam long id) throws ServiceException {
        authorService.makeExpired(id);

        return REDIRECT_AUTHORS_LIST_PARAM;
    }

    @RequestMapping(value = "/add-author", method = RequestMethod.POST)
    public String addAuthor(@Valid @ModelAttribute("authorForCreating") Author author,
                            BindingResult bindingResult, RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.authorForCreating", bindingResult);
            redirectAttributes.addFlashAttribute(AUTHOR_FOR_CREATING_PARAM, author);
            return REDIRECT_AUTHORS_LIST_PARAM;
        }

        authorService.create(author);

        return REDIRECT_AUTHORS_LIST_PARAM;
    }
}
