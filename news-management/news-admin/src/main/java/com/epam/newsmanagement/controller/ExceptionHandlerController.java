package com.epam.newsmanagement.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
/**
 * Created by Daria on 24.04.2016.
 */
@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger logger = LogManager.getRootLogger();

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e) {
        logger.error(e);
        return "error";
    }
}
