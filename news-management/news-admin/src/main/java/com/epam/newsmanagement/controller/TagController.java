package com.epam.newsmanagement.controller;

import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 13/04/2016.
 */
@Controller
public class TagController {

    private static final String TAGS_LIST_PARAM = "tagsList";
    private static final String TAG_PARAM = "tag";
    private static final String TAG_FOR_CREATING_PARAM = "tagForCreating";
    private static final String TAG_ID_PARAM = "tagId";

    private static final String TAGS_LIST_VIEW_NAME = "tags_list";
    private static final String REDIRECT_TAGS_LIST_PARAM = "redirect:/tags-list";

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/tags-list", method = RequestMethod.GET)
    public ModelAndView tagsList(Model model) throws ServiceException {
        List<Tag> tagsList = tagService.readListAll();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(TAGS_LIST_PARAM, tagsList);
        if (!model.containsAttribute(TAG_PARAM)) {
            modelAndView.addObject(TAG_PARAM, new Tag());
        }
        if (!model.containsAttribute(TAG_FOR_CREATING_PARAM)) {
            modelAndView.addObject(TAG_FOR_CREATING_PARAM, new Tag());
        }
        modelAndView.setViewName(TAGS_LIST_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(value = "/update-tag", method = RequestMethod.POST)
    public String updateTag(@Valid @ModelAttribute Tag tag, BindingResult bindingResult,
                            RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.tag", bindingResult);
            redirectAttributes.addFlashAttribute(TAG_PARAM, tag);
            redirectAttributes.addFlashAttribute(TAG_ID_PARAM, tag.getTagId());
            return REDIRECT_TAGS_LIST_PARAM;
        }

        tagService.update(tag);

        return REDIRECT_TAGS_LIST_PARAM;
    }

    @RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
    public String deleteTag(@RequestParam long id) throws ServiceException {
        tagService.delete(id);

        return REDIRECT_TAGS_LIST_PARAM;
    }

    @RequestMapping(value = "/add-tag", method = RequestMethod.POST)
    public String addTag(@Valid @ModelAttribute("tagForCreating") Tag tag,
                         BindingResult bindingResult, RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.tagForCreating", bindingResult);
            redirectAttributes.addFlashAttribute(TAG_FOR_CREATING_PARAM, tag);
            return REDIRECT_TAGS_LIST_PARAM;
        }

        tagService.create(tag);

        return REDIRECT_TAGS_LIST_PARAM;
    }
}
