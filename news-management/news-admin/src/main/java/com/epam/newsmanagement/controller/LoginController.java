package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Darya_Yeuzhyk on 04/04/2016.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/signin", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView signin(
            @RequestParam(value = "error", required = false) String error) {

        ModelAndView model = new ModelAndView();

        if (error != null) {
            model.addObject("error", "Wrong login or password.");
        }

        model.setViewName("signin");

        return model;
    }
}
