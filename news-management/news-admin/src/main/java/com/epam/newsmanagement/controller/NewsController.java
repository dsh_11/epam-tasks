package com.epam.newsmanagement.controller;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import by.epam.task1.service.CommentService;
import by.epam.task1.service.NewsTOService;
import by.epam.task1.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Darya_Yeuzhyk on 05/04/2016.
 */
@Controller
public class NewsController {

    private static final String PAGES_NUMBER_PARAM = "pagesNumber";
    private static final String CURRENT_PAGE_PARAM = "currentPage";
    private static final String NEWS_LIST_PARAM = "newsList";
    private static final String SEARCH_CRITERIA_PARAM = "criteria";
    private static final String AUTHORS_LIST_PARAM = "authorsList";
    private static final String TAGS_LIST_PARAM = "tagsList";
    private static final String NEWS_PARAM = "news";
    private static final String COMMENT_PARAM = "comment";
    private static final String NEWS_AUTHOR_PARAM = "newsAuthor";
    private static final String NEWS_TAGS_PARAM = "newsTags";

    private static final String NEWS_LIST_VIEW_NAME = "news_list";
    private static final String NEWS_VIEW_NAME = "news";
    private static final String NEWS_CREATE_VIEW_NAME = "news_create";
    private static final String NEWS_EDIT_VIEW_NAME = "news_edit";
    private static final String REDIRECT_NEWS_VIEW_NAME = "redirect:/news/";
    private static final String REDIRECT_MAIN_VIEW_NAME = "redirect:/";
    private static final String REDIRECT_NEWS_EDIT_VIEW_NAME = "redirect:/news/?/edit";

    @Autowired
    private NewsTOService newsTOService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @RequestMapping(value = { "/news-list/{page:\\d+}"}, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView newsList(@PathVariable int page, HttpSession session) throws ServiceException {
        SearchCriteriaTO criteria = (SearchCriteriaTO)session.getAttribute(SEARCH_CRITERIA_PARAM);

        int pagesNumber = newsTOService.getPagesNumber();
        List<NewsTO> newsList;
        if (criteria == null) {
            newsList = newsTOService.readList(page);
        } else {
            newsList = newsTOService.search(criteria, page);
        }

        List<Author> authorsList = authorService.readListNotExpired();
        List<Tag> tagsList = tagService.readListAll();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(PAGES_NUMBER_PARAM, pagesNumber);
        modelAndView.addObject(CURRENT_PAGE_PARAM, page);
        modelAndView.addObject(NEWS_LIST_PARAM, newsList);
        modelAndView.addObject(SEARCH_CRITERIA_PARAM, new SearchCriteriaTO());
        modelAndView.addObject(AUTHORS_LIST_PARAM, authorsList);
        modelAndView.addObject(TAGS_LIST_PARAM, tagsList);
        modelAndView.setViewName(NEWS_LIST_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(value = { "/", "/news-list"}, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView newsListDefault(HttpSession session) throws ServiceException {
        return newsList(1, session);
    }

    @RequestMapping(value = "/news/{id:\\d+}", method = RequestMethod.GET)
    public ModelAndView news(@PathVariable long id, Model model) throws ServiceException {
        NewsTO news = newsTOService.read(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(NEWS_PARAM, news);
        if (!model.containsAttribute(COMMENT_PARAM)) {
            modelAndView.addObject(COMMENT_PARAM, new Comment());
        }
        modelAndView.setViewName(NEWS_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(value = "/news/add-comment", method = RequestMethod.POST)
    public String addComment(@Valid @ModelAttribute Comment comment, BindingResult bindingResult,
                             RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.comment", bindingResult);
            redirectAttributes.addFlashAttribute(COMMENT_PARAM, comment);
            return REDIRECT_NEWS_VIEW_NAME + comment.getNewsId();
        }

        comment.setCreationDate(new Date());
        commentService.create(comment);

        return REDIRECT_NEWS_VIEW_NAME + comment.getNewsId();
    }

    @RequestMapping(value = "/news/delete-comment", method = RequestMethod.POST)
    public String deleteComment(@RequestParam long commentId, @RequestParam long newsId) throws ServiceException {
        commentService.delete(commentId);

        return REDIRECT_NEWS_VIEW_NAME + newsId;
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public ModelAndView newsAddForm(Model model) throws ServiceException {
        List<Author> authorsList = authorService.readListNotExpired();
        List<Tag> tagsList = tagService.readListAll();

        ModelAndView modelAndView = new ModelAndView();
        if (!model.containsAttribute(NEWS_PARAM)) {
            modelAndView.addObject(NEWS_PARAM, new News());
        }
        modelAndView.addObject(AUTHORS_LIST_PARAM, authorsList);
        modelAndView.addObject(TAGS_LIST_PARAM, tagsList);
        modelAndView.setViewName(NEWS_CREATE_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(value = "/add-news", method = RequestMethod.POST)
    public String addNews(@RequestParam Long authorId, @RequestParam(required = false) long[] tagsIds,
                          @Valid @ModelAttribute News news, BindingResult bindingResult,
                          RedirectAttributes redirectAttributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.news", bindingResult);
            redirectAttributes.addFlashAttribute(NEWS_PARAM, news);
            return REDIRECT_NEWS_VIEW_NAME;
        }
        NewsTO newsTO = new NewsTO();
        List<Tag> tags = new ArrayList<>();
        Author author = new Author();
        author.setAuthorId(authorId);
        if (tagsIds != null) {
            for (Long tagId : tagsIds) {
                Tag tag = new Tag();
                tag.setTagId(tagId);
                tags.add(tag);
            }
        }
        Date date = new Date();
        news.setCreationDate(date);
        news.setModificationDate(date);
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);

        Long newsId = newsTOService.create(newsTO);

        return REDIRECT_NEWS_VIEW_NAME + newsId;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@ModelAttribute SearchCriteriaTO criteria, HttpSession session) {
        session.setAttribute(SEARCH_CRITERIA_PARAM, criteria);
        return REDIRECT_MAIN_VIEW_NAME;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public String reset(HttpSession session) {
        session.removeAttribute(SEARCH_CRITERIA_PARAM);
        return REDIRECT_MAIN_VIEW_NAME;
    }

    @RequestMapping(value = "/news/{id:\\d+}/edit", method = RequestMethod.GET)
    public ModelAndView newsEditForm(@PathVariable long id, Model model) throws ServiceException {
        NewsTO news = newsTOService.read(id);
        List<Author> authorsList = authorService.readListNotExpired();
        List<Tag> tagsList = tagService.readListAll();

        ModelAndView modelAndView = new ModelAndView();
        if (!model.containsAttribute(NEWS_PARAM)) {
            modelAndView.addObject(NEWS_PARAM, news.getNews());
        }
        modelAndView.addObject(NEWS_AUTHOR_PARAM, news.getAuthor());
        modelAndView.addObject(NEWS_TAGS_PARAM, news.getTags());
        modelAndView.addObject(AUTHORS_LIST_PARAM, authorsList);
        modelAndView.addObject(TAGS_LIST_PARAM, tagsList);
        modelAndView.setViewName(NEWS_EDIT_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(value = "/edit-news", method = RequestMethod.POST)
    public String edit(@RequestParam long authorId, @RequestParam(required = false) long[] tagsIds,
                       @Valid @ModelAttribute News news, BindingResult bindingResult,
                       RedirectAttributes redirectAttributes) throws ServiceException {
        NewsTO newsTO = new NewsTO();
        Author author = new Author();
        author.setAuthorId(authorId);
        List<Tag> tags = new ArrayList<>();
        if (tagsIds != null) {
            for (long tagId : tagsIds) {
                Tag tag = new Tag();
                tag.setTagId(tagId);
                tags.add(tag);
            }
        }
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.news", bindingResult);
            redirectAttributes.addFlashAttribute(NEWS_PARAM, news);
            return REDIRECT_NEWS_EDIT_VIEW_NAME.replaceFirst("\\?", news.getNewsId().toString());
        }
        news.setModificationDate(new Date());
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);

        newsTOService.update(newsTO);

        return REDIRECT_MAIN_VIEW_NAME;
    }

    @RequestMapping(value = "delete-news", method = RequestMethod.POST)
    public String delete(@RequestParam(required = false) long[] newsIds) throws ServiceException {
        if (newsIds != null) {
            for (long newsId : newsIds) {
                newsTOService.delete(newsId);
            }
        }
        return REDIRECT_MAIN_VIEW_NAME;
    }
}
