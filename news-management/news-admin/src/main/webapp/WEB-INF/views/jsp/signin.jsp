<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="signin">
	<form name="loginForm" action="<c:url value='j_spring_security_check'/>" method="POST">
		<table>
			<tr>
				<td>Login:</td>
				<td><input type='text' name='login' value=''></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type='password' name='password' /></td>
			</tr>
			<c:if test="${not empty error}">
				<tr>
					<td style="color: red;">${error}</td>
				</tr>
			</c:if>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
									   value="submit" /></td>
			</tr>
		</table>

		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>