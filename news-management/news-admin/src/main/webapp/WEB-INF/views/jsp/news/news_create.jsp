<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/multiple-select.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/news-create.css" />
<script src="${pageContext.request.contextPath}/resources/js/multiple-select.js"></script>

<form:form action="${pageContext.request.contextPath}/add-news" method="POST"
			modelAttribute="news" cssClass="news-create-form">
	<div class="news-create-form-block">
		<form:label path="title">Title:</form:label>
		<form:input path="title"/>
		<form:errors path="title" cssClass="error"/>
	</div>
	<div class="news-create-form-block">
		<form:label path="shortText">Description:</form:label>
		<form:textarea rows="5" path="shortText"/>
		<form:errors path="shortText" cssClass="error"/>
	</div>
	<div class="news-create-form-block">
		<form:label path="fullText">News text:</form:label>
		<form:textarea rows="5" path="fullText"/>
		<form:errors path="fullText" cssClass="error"/>
	</div>
	<div class="news-create-select-block">
		<select name="authorId">
			<c:forEach items="${authorsList}" var="author">
				<option value="${author.authorId}">${author.authorName}</option>
			</c:forEach>
		</select>
		<select id="tags" name="tagsIds" multiple style="width: 150px;">
			<c:forEach items="${tagsList}" var="tag">
				<option value="${tag.tagId}">${tag.tagName}</option>
			</c:forEach>
		</select>
	</div>
	<form:hidden path="newsId"/>
	<input class="news-create-save-button" type="submit" value="Save"/>
</form:form>

<script>
	$('#tags').multipleSelect();
</script>
