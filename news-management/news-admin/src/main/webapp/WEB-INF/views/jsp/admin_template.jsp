<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<html>
	<head>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css" />
		<script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.3.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/logout.js"></script>
	</head>
	<body>
		<div class="header">
		    <tiles:insertAttribute name="header" />
		</div>

		<div class="menu">
			<tiles:insertAttribute name="menu" />
		</div>

		<div class="content">
		    <tiles:insertAttribute name="content" />
		</div>

		<div class="footer">
		    <tiles:insertAttribute name="footer" />
		</div>
	</body>
</html>