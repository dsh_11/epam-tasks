<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/multiple-select.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pagination.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/news-list.css"/>
<script src="${pageContext.request.contextPath}/resources/js/multiple-select.js"></script>

<div class="news-list-search">
	<form:form action="${pageContext.request.contextPath}/search" method="POST"
			   modelAttribute="criteria">
		<form:select path="authorId">
			<form:option value="">Select author</form:option>
			<form:options items="${authorsList}" itemLabel="authorName"
						  itemValue="authorId"/>
		</form:select>
		<form:select cssStyle="width: 150px" id="tags" path="tagIds" items="${tagsList}"
					 itemLabel="tagName" itemValue="tagId"/>
		<input type="submit" value="Search"/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form:form>
	<form action="${pageContext.request.contextPath}/reset" method="POST">
		<input type="submit" value="Reset"/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>

<form action="${pageContext.request.contextPath}/delete-news" method="POST">
	<ul style="list-style-type: none;">
		<c:forEach items="${newsList}" var="news">
			<li class="news-item">
				<div>
					<div>
						<ul style="padding-left: 0;">
							<li class="news-title">
								<a class="news-title" href="${pageContext.request.contextPath}/news/${news.news.newsId}">
								${news.news.title}
								</a>
							</li>
							<li class="author-name">(by ${news.author.authorName})</li>
							<li class="date">
								<fmt:formatDate type="date" value="${news.news.creationDate}" />
							</li>
						</ul>
					</div>
					<div>
						<span>${news.news.shortText}</span>
					</div>

					<div align="right" style="margin-right: 30px;">
						<ul class="news-bottom">
							<c:if test="${not empty news.tags}">
								<li>
									<ul>
										<c:forEach items="${news.tags}" var="tag">
											<li>${tag.tagName}</li>
										</c:forEach>
									</ul>
								</li>
							</c:if>
							<li style="margin: 0 5px 0 10px;">
								Comments(${fn:length(news.comments)})
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/news/${news.news.newsId}/edit">Edit</a>
							</li>
							<li>
								<input name="newsIds" type="checkbox" value="${news.news.newsId}"/>
							</li>
						</ul>
					</div>
				</div>
			</li>
		</c:forEach>
	</ul>
	<input type="submit" value="Delete" class="news-delete-button"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

<div style="clear: both" class="pages">
	<ul class="pagination">
		<c:forEach begin="1" end="${pagesNumber}" var="i">
			<c:choose>
				<c:when test="${currentPage == i}">
					<li>
						<span id="page-active">${i}</span>
					</li>
				</c:when>
				<c:otherwise>
					<li>
						<a id="page-inactive" href="${pageContext.request.contextPath}/news-list/${i}">${i}</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</ul>
</div>

<script>
	$('#tags').multipleSelect();
</script>
