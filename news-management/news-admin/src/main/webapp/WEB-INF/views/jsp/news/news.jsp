<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/news.css" />

<div class="news">
	<h4>${news.news.title}</h4>
	<div id="author-name">(by ${news.author.authorName})</div>
	<div id="creation-date"><fmt:formatDate type="date" value="${news.news.creationDate}" /></div>
	<div>
		<p>
			${news.news.fullText}
		</p>
	</div>
	<div>
		<form:form action="${pageContext.request.contextPath}/news/add-comment" method="POST"
					modelAttribute="comment">
			<div>
				<form:textarea path="commentText" id="comment-area" rows="10" cols="40"/>
			</div>
			<div>
				<form:errors path="commentText" cssClass="error"/>
			</div>
			<div>
				<input name="submit" type="submit" value="Post comment"/>
			</div>
			<form:hidden path="newsId" value="${id}"/>
		</form:form>
	</div>
	<c:if test="${not empty news.comments}">
		<div>
			<ul class="comments">
				<c:forEach items="${news.comments}" var="comment">
					<li class="comment-item">
						<div>
							<fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
						</div>
						<div>
							<div class="comment-delete">
								<form action="${pageContext.request.contextPath}/news/delete-comment"
										method="POST">
									<input type="submit" value="X"/>
									<input type="hidden" name="commentId" value="${comment.commentId}"/>
									<input type="hidden" name="newsId" value="${id}"/>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								</form>
							</div>
							<div class="comment">
								${comment.commentText}
							</div>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
	</c:if>
</div>