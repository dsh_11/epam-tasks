<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/multiple-select.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/news-edit.css" />
<script src="${pageContext.request.contextPath}/resources/js/multiple-select.js"></script>

<form:form action="${pageContext.request.contextPath}/edit-news" method="POST"
      cssClass="news-edit-form" modelAttribute="news">
    <div class="news-edit-form-block">
        <form:label path="title" for="title">Title:</form:label>
        <form:input path="title" name="title" id="title" type="text"/>
        <form:errors path="title" cssClass="error"/>
    </div>
    <div class="news-edit-form-block">
        <form:label path="shortText" for="shortText">Description:</form:label>
        <form:textarea path="shortText" rows="5" name="shortText" id="shortText"/>
        <form:errors path="shortText" cssClass="error"/>
    </div>
    <div class="news-edit-form-block">
        <form:label path="fullText" for="fullText">News text:</form:label>
        <form:textarea path="fullText" rows="5" name="fullText" id="fullText"/>
        <form:errors path="fullText" cssClass="error"/>
    </div>
    <div class="news-create-select-block">
        <select name="authorId">
            <c:forEach items="${authorsList}" var="author">
                <c:if test="${author.authorId == newsAuthor.authorId}">
                    <option value="${author.authorId}" selected>${author.authorName}</option>
                </c:if>
                <c:if test="${author.authorId != newsAuthor.authorId}">
                    <option value="${author.authorId}">${author.authorName}</option>
                </c:if>
            </c:forEach>
        </select>
        <select id="tags" name="tagsIds" multiple style="width: 150px;">
            <c:forEach items="${tagsList}" var="tag">
                <c:if test="${fn:contains(newsTags, tag)}">
                    <option value="${tag.tagId}" selected>${tag.tagName}</option>
                </c:if>
                <c:if test="${not fn:contains(newsTags, tag)}">
                    <option value="${tag.tagId}">${tag.tagName}</option>
                </c:if>
            </c:forEach>
        </select>
    </div>
    <form:hidden path="newsId"/>
    <input class="news-create-save-button" type="submit" value="Save"/>
</form:form>

<script>
    $('#tags').multipleSelect();
</script>
