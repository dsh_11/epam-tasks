<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/authors-list.css" />

<ul class="authors-list">
	<c:forEach items="${authorsList}" var="currentAuthor">
		<li class="author-item">
			<form:form action="${pageContext.request.contextPath}/update-author" method="POST"
					   modelAttribute="author" cssClass="author-update-form">
				<form:label path="authorName" cssClass="author-update-label">Author:</form:label>
				<form:input path="authorName" cssClass="author-update-name-input" value="${currentAuthor.authorName}"/>
				<form:hidden path="authorId" value="${currentAuthor.authorId}"/>
				<c:if test="${currentAuthor.authorId eq authorId}">
					<form:errors path="authorName" cssClass="error"/>
				</c:if>
				<input class="author-update-button" type="submit" value="update" />
			</form:form>
			<c:if test="${currentAuthor.expired == null}">
				<form class="author-expire-form" action="${pageContext.request.contextPath}/expire-author" method="POST">
					<input class="author-expire-button" type="submit" value="expire" />
					<input type="hidden" name="id" value="${currentAuthor.authorId}"/>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
			</c:if>
		</li>
	</c:forEach>
	<li class="add-author-item">
		<form:form action="${pageContext.request.contextPath}/add-author" method="POST"
					modelAttribute="authorForCreating">
			<form:label path="authorName" cssClass="author-add-label">Add author:</form:label>
			<form:input path="authorName" cssClass="author-add-name-input"/>
			<input class="author-add-button" type="submit" value="add" />
			<div>
				<form:errors path="authorName" cssClass="error"/>
			</div>
		</form:form>
	</li>
</ul>