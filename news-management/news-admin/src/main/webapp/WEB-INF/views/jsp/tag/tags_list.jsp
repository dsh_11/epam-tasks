<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/tags-list.css" />

<ul class="tags-list">
    <c:forEach items="${tagsList}" var="currentTag">
        <li class="tag-item">
            <form:form cssClass="tag-update-form" action="${pageContext.request.contextPath}/update-tag"
                       method="POST" modelAttribute="tag">
                <form:label path="tagName" cssClass="tag-update-label">Tag:</form:label>
                <form:input path="tagName" cssClass="tag-update-name-input" value="${currentTag.tagName}"/>
                <form:hidden path="tagId" value="${currentTag.tagId}"/>
                <c:if test="${currentTag.tagId eq tagId}">
                    <form:errors path="tagName" cssClass="error"/>
                </c:if>
                <input class="tag-update-button" type="submit" value="update" />
            </form:form>
            <form class="tag-delete-form" action="${pageContext.request.contextPath}/delete-tag" method="POST">
                <input class="tag-delete-button" type="submit" value="delete" />
                <input type="hidden" name="id" value="${currentTag.tagId}"/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
        </li>
    </c:forEach>
    <li class="add-tag-item">
        <form:form action="${pageContext.request.contextPath}/add-tag" method="POST"
                    modelAttribute="tagForCreating">
            <form:label path="tagName" class="tag-add-label">Add tag:</form:label>
            <form:input path="tagName" class="tag-add-name-input"/>
            <input class="tag-add-button" type="submit" value="add" />
            <div>
                <form:errors path="tagName" cssClass="error"/>
            </div>
        </form:form>
    </li>
</ul>