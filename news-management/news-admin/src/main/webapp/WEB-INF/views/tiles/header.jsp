<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<div>
    <div style="display: inline-block; padding-left: 30px;">
        <h1>News portal - Administration</h1>
    </div>
    <div style="display: inline-block; float: right">
        <c:url value="/logout" var="logoutUrl" />

        <form action="${logoutUrl}" method="POST" id="logoutForm">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <div>
                <span>Hello, ${pageContext.request.userPrincipal.name}</span>
                <a href="javascript:formSubmit()">Logout</a>
            </div>
        </c:if>

        <div>
            <a href="">EN</a>
            <a href="">RU</a>
        </div>
    </div>
</div>
